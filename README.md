# README: Donor generation: project of automated software transplantation for SPL
MAINTAINER Leandro Souza "leouneb@gmail.com"

This repository has a fragment of our program responsible for remove all conditional directives from our target system, in this case, the MacVim text editor. We are interested in remove all remove preprocessor conditionals from code in order to create a compilable version of MacVim without all unnecessary portion of code. We have been using UNIFDEF command (https://linux.die.net/man/1/unifdef), however, the output file is not able to be compilable. Below we describe the structure of this repository, how to running our example and the errors showed. 

# REPOSITORY STRUCTURE

    donor_generation
    |_ macvim  (donor main folder)
        |_ ...
    |_ TempDonorFiles (output folder)
        |_ ...
    |_ list_conditional_directives.in (list of conditional directives which will be removed)
    |_ ListOfString.h
    |_ remove_directives (program responsable for remove directives from macvim)
    |_ remove_directives.c (source code responsable for remove directives from macvim)

# STEPS

1) Execute the remove_directives program: 

         RUN:  ./remove_directives
         This program will copy all *.c and *.h files from macvim to TempDonorFiles/macvim/src (our target source code without the directives listed in list_conditional_directives.in) 
    
    If you make any change in our program, please compile it using:
                
        RUN: gcc -o remove_directives remove_directives
        
2) Compile our target system from TempDonorFiles/macvim/

        RUN: make


# ERROR MESSAGE:

        ...
        config.status: creating auto/config.h
        /Applications/Xcode.app/Contents/Developer/usr/bin/make -f Makefile all
        /bin/sh install-sh -c -d objects
        install-sh: line 1: /Applications: is a directory
        install-sh: line 2: GvimExt: command not found
        install-sh: line 3: GvimExt: command not found
        install-sh: line 4: GvimExt: command not found
        install-sh: line 5: GvimExt: command not found
        install-sh: line 6: GvimExt: command not found
        install-sh: line 7: GvimExt/: is a directory
        make[2]: *** [objects/.dirstamp] Error 126
        make[1]: *** [reconfig] Error 2
        make: *** [first] Error 2
    
