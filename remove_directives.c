/* 
 * File:   main.c
 * Author: alex
 *
 * Created on 26 May 2014, 03:56
 */

//
//  main.c
//  ResearchProject
//
//  Created by Marginean Alexandru on 27/02/14.
//  Copyright (c) 2014 Marginean Alexandru. All rights reserved.
//
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <sys/resource.h>

#include "ListOfString.h"

void addNewString(ListOfString ** head, char * string) {
    ListOfString * newEl = (ListOfString *) malloc(1 * sizeof (ListOfString));
    newEl->el = (char *) malloc((strlen(string) + 1) * sizeof (char *));
    strcpy(newEl->el, string);
    newEl->next = NULL;
    if (* head == NULL) {
        * head = newEl;
    } else {
        ListOfString * p;
        p = *head;
        while (p ->next) {
            p = p->next;
        }
        p ->next = newEl;
    }
}


ListOfString * readListOfStringFromFile(char * input) {
    ListOfString * head = NULL;

    FILE * fin = fopen(input, "r");
    char * line;
    line = (char *) malloc(3000 * sizeof (char));

    while (fgets(line, 2999, fin) != NULL) {
        addNewString(&head, line);
    }

    free(line);
    fclose(fin);
    return head;
}

int copyFilesFromADirToAnother(char * src, char * dest) {
    char *commandCopyInterface = NULL;
    commandCopyInterface = (char *) malloc(5000 * sizeof(char));
    sprintf(commandCopyInterface, "cp -R %s %s", src, dest);
    int commandCopyStatus = system(commandCopyInterface);

    return commandCopyStatus;

}

int removeFileFromDirectory(char * dirPath) {
    DIR *dir = opendir(dirPath);
    if (dir) {
        /* Directory exists. */
        closedir(dir);
        char *commandCleanDempDonorSourceFileFolder = NULL;
        commandCleanDempDonorSourceFileFolder = (char *) malloc(1000 * sizeof(char));
        sprintf(commandCleanDempDonorSourceFileFolder, "cd %s \n rm -r *", dirPath);
        int comandStatus = system(commandCleanDempDonorSourceFileFolder);
    } else {
        int dir_result = mkdir(dirPath, 0777);
        if (dir_result > 0) {
           return 0;
        }
    }
    return 1;
}

void copyDonorSourceFilesToTempDonorSourceFilesFolder(char * tempDonorSourceFileFolder, char * donorSourceFileFolder) {

		int removeCommandStatus = removeFileFromDirectory(tempDonorSourceFileFolder);
		if(removeCommandStatus){
			int copyFileFromDonorToTemp = copyFilesFromADirToAnother(donorSourceFileFolder, tempDonorSourceFileFolder);
			if (copyFileFromDonorToTemp) {
				printf("ERROR: Copy Ice Box Tests from donor to tempDonorSourceFile directory. \n");
				exit(1);
			}
		}else{
			printf("ERROR: Remove files at tempDonorSourceFileFolder.\n");
			exit(1);
		}
}

void readFilesFromDirWithOutQuotationMarks(char *path, FILE *fout, char endChar) {
    DIR *d;
    struct dirent *dir;
    char * newPath;
    newPath = (char*) malloc(500 * sizeof (char));
    strcpy(newPath, path);
    d = opendir(path);
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == 4 && strcmp(".", dir->d_name) && strcmp("..", dir->d_name)) {


                //void readDirContent(char * path, char** listOfNeededHeaderFiles, int length, char * outputFolder)
                strcat(newPath, dir->d_name);
                strcat(newPath, "/");
                readFilesFromDirWithOutQuotationMarks(newPath, fout, endChar);
            } else if (dir->d_name[strlen(dir->d_name) - 1] == endChar) {
                fprintf(fout, "%s%s\n", path, dir->d_name);
                //printf("\"%s%s\" \n", path, dir->d_name);
            }
        }
        closedir(d);
    }
    free(newPath);
}

int main(){

	char * donorSourceFileFolder = "macvim/src/";
	char * listOfConditionalDirectivesPath = "list_conditional_directives.in";
	char * tempDonorSourceFileFolder = "TempDonorFiles/macvim/src/";

	printf("Copying files from donor folder...\n");
    copyDonorSourceFilesToTempDonorSourceFilesFolder(tempDonorSourceFileFolder, donorSourceFileFolder);

	printf("[UNIDEF] Removing unwanted conditional directives...\n");
    /* create new donor temporary directory */
    char * sourceFileList = (char *) malloc(400 * sizeof(char));
    sprintf(sourceFileList, "%stempSourceFiles.out", tempDonorSourceFileFolder);
    FILE * fSourceFileList = fopen(sourceFileList, "w");
	readFilesFromDirWithOutQuotationMarks(tempDonorSourceFileFolder, fSourceFileList, 'c');
	readFilesFromDirWithOutQuotationMarks(tempDonorSourceFileFolder, fSourceFileList, 'h');
    fclose(fSourceFileList);

	char * donorSourceFileList = (char *) malloc(400 * sizeof(char));
	sprintf(donorSourceFileList, "%sdonorSourceFiles.out", tempDonorSourceFileFolder);
	FILE * fDonorSourceFileList = fopen(donorSourceFileList, "w");
	readFilesFromDirWithOutQuotationMarks(donorSourceFileFolder, fDonorSourceFileList, 'c');
	readFilesFromDirWithOutQuotationMarks(donorSourceFileFolder, fDonorSourceFileList, 'h');
	fclose(fDonorSourceFileList);

    /* remove directives */
    char * unifdefCommand;
    unifdefCommand = (char *) malloc(20000 * sizeof(char));
    strcpy(unifdefCommand, "unifdef");

    ListOfString *listOfSourceFiles = readListOfStringFromFile(sourceFileList);
    ListOfString *listOfConditionalDirectives = readListOfStringFromFile(listOfConditionalDirectivesPath);

	int cont =0;

    while(listOfConditionalDirectives){
    	printf("Directives to be removed\n");
        if ((strstr(listOfConditionalDirectives->el, "-U") || strstr(listOfConditionalDirectives->el, "-D")) && (!strstr(listOfConditionalDirectives->el, "%%") && !strstr(listOfConditionalDirectives->el, "/*") && !strstr(listOfConditionalDirectives->el, "    "))){
          //  printf("%s",listOfConditionalDirectives->el);
            strcat(unifdefCommand, " ");
            strtok(listOfConditionalDirectives->el, "\n");
            printf("%s\n",listOfConditionalDirectives->el);
            strcat(unifdefCommand, listOfConditionalDirectives->el);

        }
        listOfConditionalDirectives = listOfConditionalDirectives->next;
        cont++;
    }
	//strcat(unifdefCommand,listOfConditionalDirectives->el);
	//printf("\n%s\n",unifdefCommand);
    char * line;
    line = (char *) malloc(1000 * sizeof(char));
    char * currentFilePath;
    currentFilePath = (char *) malloc(1000 * sizeof(char));



    FILE * fin = fopen(donorSourceFileList, "r");

	printf("[UNIDEF] Removing unwanted conditional directives...\n");
    while (fgets(line, 1000, fin) != NULL) {
		char * endCommandFiles=NULL;
		endCommandFiles = (char *) malloc(100000 * sizeof(char));
        sscanf(line, "%s", currentFilePath);

        if (strstr(currentFilePath, ".c") || strstr(currentFilePath, ".h")) {
            printf("\tFile: %s\n", currentFilePath);
            //strcpy(endCommandFiles, unifdefCommand);
            sprintf(endCommandFiles, "%s %s > %s", unifdefCommand, currentFilePath, listOfSourceFiles->el);
            //printf("\tEndCommandFiles: %s\n", endCommandFiles);
            int status = system(endCommandFiles);
            if (status == 2) {
                printf("ERROR: %d. System could conditional directive(s) from file: %s !!!!!\n", status,
                       currentFilePath);
            } else {
                listOfSourceFiles = listOfSourceFiles->next;
            }
            fflush(stdout);
        }
    }

return 0;
}